extends Control

func _ready():
	# Установка минимального размера окна
	var min_size = Vector2(800, 600) # Замените на желаемый размер
	ProjectSettings.set_setting("display/window/size/min_width", min_size.x)
	ProjectSettings.set_setting("display/window/size/min_height", min_size.y)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
