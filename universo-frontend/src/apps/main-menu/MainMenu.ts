import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';

function CreateMainMenu(scene: EndlessCanvas, buttonArray) {
  const mainMenuHeight = scene.mainMenuHeight; // Ширина главного меню
  const arr = [];
  buttonArray.forEach((element, index) => {
    const button = scene.add.image(0, 0, element.image).setDepth(1010);
    button.setScale(mainMenuHeight / button.height);
    button.setPosition(
      button.displayWidth / 2 + index * mainMenuHeight + index * 10 + 5,
      button.displayHeight / 2 + 5,
    );
    button.setInteractive({ cursor: 'pointer' });

    button.on('pointerdown', element.callback);
    arr.push(button);
  });

  return arr;
}
export default CreateMainMenu;
